exports.config = {
    seleniumAddress: 'http://hub.browserstack.com/wd/hub',
    baseUrl: 'http://qa.bpscore.com',
    multiCapabilities: [{
        'browserName': 'chrome',
        'version': '36.0',
        'os': 'OS X',
        'os_version': 'Yosemite',
        'browserstack.user': 'hermanocabral1',
        'browserstack.key': 'KP1EyNVcJ18xiBvsSx9R',
        'browserstack.debug': 'true',
        'browserstack.local': 'true'
    }],
    specs: ['*.spec.js'],

    jasmineNodeOpts: {defaultTimeoutInterval: 500000}
};
