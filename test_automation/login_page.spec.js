var Page = function () {
    this.usernameField = element(by.model('username'));
    this.passwordField = element(by.model('password'));
    this.loginButton = element(by.css('.login-button'));
    this.title = element(by.css('.dashboard-title'));
    
    this.get = function() {
        browser.get('/#/login')    
    }
    
    this.cleanFields = function () {
        this.usernameField.clear().then();
        this.passwordField.clear().then();
    }
    
    this.getErrorText = function () {
        return element(by.css('.error-message')).getText();
    }
    
    this.setUsername = function (name) {
        this.usernameField.sendKeys(name);
    }
    
    this.setPassword = function (password) {
        this.passwordField.sendKeys(password);
    }
    
    this.getTitleText = function () {
        return this.title.getText();
    }
}

describe('Business Pulse login page', function () {

    var loginPage = new Page();
    
    beforeEach(function () {
        loginPage.get();
    });

    it('should load the login page', function () {
        expect(loginPage.getTitleText()).toEqual('Dashboard Login');
    });

    it('should require a password to access the system', function () {
        loginPage.setUsername('pstest');
        
        loginPage.loginButton.click();

        var error = loginPage.getErrorText();

        expect(error).toEqual('Username and password are required');
    });

    it('should require a username to access the system', function () {
        loginPage.setPassword('pstest');
        
        loginPage.loginButton.click();

        var error = loginPage.getErrorText();

        expect(error).toEqual('Username and password are required');
    });

    it('should refuse access to invalid credentials', function () {
        loginPage.setUsername('pstest');
        loginPage.setPassword('pstest');
        
        loginPage.loginButton.click();

        var error = loginPage.getErrorText();

        expect(error).toEqual('Unable to log in with provided credentials.');
    });

    it('should require valid credentials to load the dashboard', function () {
        loginPage.setUsername('verizon');
        loginPage.setPassword('verizontemp');

        loginPage.loginButton.click();
        
        browser.driver.wait(function() {
            return browser.driver.isElementPresent(by.css(".dash-picker"));
        });
        
        expect(loginPage.getTitleText()).toEqual('Communications Industry');
    });
});
